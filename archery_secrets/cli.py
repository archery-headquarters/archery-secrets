from cryptography.fernet import Fernet
from click import (
    group, command, option, argument, echo
)
from archery_secrets import Secret


secret = Secret()

@group()
def cli():
    ...

@command(help="Generate new key.")
def new() -> str:
    echo(secret.new)

@command(help='Hash text')
@argument('text')
def hash(text: str) -> str:
    echo(
        secret.hash(
            text
        )
    )

@command(help='Verify hash and passphrase.')
@argument('hash')
@argument('text')
def verify(hash: str, text: str) -> bool:
    echo(
        secret.verify(hash, text)
    )

@command(help='Encrypt text.')
@argument('text')
def crypt(text: str) -> str:
    echo(
        secret.crypt(text)
    )

@command(help='Decrypt text.')
@argument('cypher')
def decrypt(cypher: str) -> str:
    echo(
        secret.decrypt(cypher)
    )

cli.add_command(new)
cli.add_command(hash)
cli.add_command(verify)
cli.add_command(crypt)
cli.add_command(decrypt)
