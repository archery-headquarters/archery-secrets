# Archer Secrets
Secrets for the Archery ecosystem.

To use this package, the following environment variable is required:
* ARCHERY_SECRET

One way to get a valid key for this environment variable is to use the cryptography package:

```python
from cryptography.fernet import Fernet
Fernet.generate_key().decode()
```

### How to use
```python
from archery_secrets import Secret
secret = Secret()
new_key = secret.new
hash = secret.hash('V3ry57r0n6p4s5w0rd4n07h1n6')
print('password passed!') if secret.verify(hash, 'V3ry57r0n6p4s5w0rd4n07h1n6') else print('Wrong password!')
message = secret.crypt('Top secret message.')
print('Decrypted message!') if secret.decrypt(message) == 'Top secret message.' else print('It is bad!')
```

It is possible to make use of the command line as a module:
```bash
$ python -m archery_secrets --help
Usage: python -m archery_secrets [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  crypt    Encrypt text.
  decrypt  Decrypt text.
  hash     Hash text
  new      Generate new key.
  verify   Verify hash and passphrase.
```
