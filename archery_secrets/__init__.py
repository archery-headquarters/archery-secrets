from os import getenv
from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from cryptography.fernet import Fernet
from archery_exceptions import EnvVarError


class Secret:
    __hash = PasswordHasher()
    if getenv('ARCHERY_SECRET'):
        __crypt = Fernet(getenv('ARCHERY_SECRET'))
    else:
        raise EnvVarError('ARCHERY_SECRET')

    @property    
    def new(self: object) -> str:
        return Fernet.generate_key().decode()

    def hash(self: object, text: str) -> str:
        return self.__hash.hash(text)

    def verify(self:object, hash: str, text: str) -> bool:
        try:
            return self.__hash.verify(hash, text)
        except VerifyMismatchError:
            return False

    def crypt(self: object, text: str) -> str:
        return self.__crypt.encrypt(text.encode()).decode()

    def decrypt(self: object, cypher:str) -> str:
        return self.__crypt.decrypt(cypher).decode()
